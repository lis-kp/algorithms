import java.util.HashSet;

/*
Leetcode Problem 3. Longest Substring Without Repeating Characters

Given a string, find the length of the longest substring without repeating characters.

Examples:

Given "abcabcbb", the answer is "abc", which the length is 3.

Given "bbbbb", the answer is "b", with the length of 1.

Given "pwwkew", the answer is "wke", with the length of 3. Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
*/

public class LongestSubstringUniqueChars {
    //approach using sliding window
    public int lengthOfLongestSubstring(String s) {
        HashSet<Character> set = new HashSet<Character>();
        
        int i = 0;
        int j = 0;
        int longest = 0;
        
        while(i < s.length() && j < s.length()){
            if (!set.contains(s.charAt(j))){
                set.add(s.charAt(j++));
                longest = Math.max(longest, j-i);
            }else{
                set.remove(s.charAt(i++));
            }
            
        }
        return longest;
    }
}