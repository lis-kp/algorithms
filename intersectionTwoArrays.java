import java.util.ArrayList;
import java.util.Arrays;

/*
Leetcode Problem 350. Intersection of Two Arrays II
Given two arrays, write a function to compute their intersection.

Example:
Given nums1 = [1, 2, 2, 1], nums2 = [2, 2], return [2, 2].

Note:
Each element in the result should appear as many times as it shows in both arrays.
The result can be in any order.
*/
public class IntersectionTwoArrays {
    public int[] intersect(int[] nums1, int[] nums2) {
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        
        int p1 =0;
        int p2 = 0;
        ArrayList<Integer> list = new ArrayList<Integer>();
        
        while(p1 <nums1.length && p2 <nums2.length){
            if (nums1[p1] < nums2[p2]){
                p1++;
            }else if (nums2[p2] < nums1[p1]){
                p2++;
            }else{
                list.add(nums1[p1]);
                p1++;
                p2++;
            }
        }
        int[] res = new int[list.size()];
        for (int i=0; i<list.size(); i++){
            res[i] = list.get(i);
        }
        return res;
    }
}